public class Apple {
	public String colour;
	public double size;
	public int longevity;
	
	public void lastProduct(){
		System.out.println("The colour is " + colour + ", the size of the apple is " + size + " centimeters, and the longevity is " + longevity + " days.");
		
	}
}