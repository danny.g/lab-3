import java.util.Scanner;
public class Shop {
public static void main (String[]args){
	Apple[] ap = new Apple[4];
	Scanner dg = new Scanner(System.in);
	for (int i = 0; i < ap.length; i++){
		ap[i] = new Apple();
			System.out.println("Enter a colour");
			ap[i].colour = dg.nextLine();
			System.out.println("Enter the size in centimeters. Decimals allowed.");
			ap[i].size = dg.nextDouble();
			System.out.println("Enter the longevity of the apple in days.");
			ap[i].longevity = dg.nextInt();
		}
	System.out.println("The last product has these following fields: \n");
	ap[3].lastProduct();
	}
}